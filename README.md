AMD Red Theme
=============

AMD Red Look & Feel package. A work in progress!

## Contents

* Color scheme
* Plasma theme
* SDDM theme, lock screen and splash screen based on Breeze.
* GTK2 and GTK3 theme based on Oomox-Yourube.

## Recommendations
* Icons: Sardi Colora Firebrick
* Cursor theme: Breeze Red Cursor Theme

## Issues

* Non at this time

![Alt text](/lookandfeel/contents/previews/preview.png?raw=true)


## Credits

* Breeze theme created by: The KDE Team
* GTK themes created by: [Bogdancovaciu](https://forum.manjaro.org/u/bogdancovaciu) from Manjaro forum
 
